import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "Latihan Pertama",
    home: new Home(),
  ));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.home),
        title: Center(
          child: Text('Sukapura Apps'),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert),
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              children: <Widget>[
                Image.asset('images/oi.jpg'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                      "Jokowi:  Umat Kristiani, Semoga Natal Membawa Kedamaian dan Kerukunan"),
                )
              ],
            ),
          ),
          Card(
            child: Column(
              children: <Widget>[
                Image.network(
                    'https://cdns.klimg.com/kapanlagi.com/p/headline/bertengkar-hebat-dengan-putri-kandungny-f48a36.jpg'),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Jokowi: Lorem Ipsum si ammet"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

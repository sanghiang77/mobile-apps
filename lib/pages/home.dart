import 'package:flutter/material.dart';

class Homes extends StatefulWidget {
  @override
  _HomesState createState() => _HomesState();
}

class _HomesState extends State<Homes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 25,
          child: Image.asset('images/sukapura.png'),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
            color: Colors.black45,
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.message),
            color: Colors.black45,
          )
        ],
        backgroundColor: Colors.white,
        elevation: 1,
        centerTitle: false,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: Colors.lightBlue[900],
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                  )),
              height: 35,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      'images/paygo.png',
                      scale: 3,
                    ),
                    Text(
                      'Rp.40.000',
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: 80,
              decoration: BoxDecoration(
                  color: Colors.lightBlue[800],
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  )),
              child: Container(
                padding: EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    HeaderItem(
                      images: 'images/pay.png',
                      title: 'Bayar',
                    ),
                    HeaderItem(
                      images: 'images/promo.png',
                      title: 'Pelajar',
                    ),
                    HeaderItem(
                      images: 'images/topup.png',
                      title: 'Isi Ulang',
                    ),
                    HeaderItem(
                      images: 'images/more.png',
                      title: 'PPOB',
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                InkWell(
                  onTap: () {},
                  child: HeaderItem(
                    images: 'images/gojek.png',
                    title: 'Ojeg',
                    textcolor: Colors.grey[600],
                  ),
                ),
                HeaderItem(
                  images: 'images/car.png',
                  title: 'Mobil',
                  textcolor: Colors.grey[600],
                ),
                HeaderItem(
                  images: 'images/gofood.png',
                  title: 'Makan',
                  textcolor: Colors.grey[600],
                ),
                HeaderItem(
                  images: 'images/wedding.png',
                  title: 'Wedding',
                  textcolor: Colors.grey[600],
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                HeaderItem(
                  images: 'images/hajatan.png',
                  title: 'Hajatan',
                  textcolor: Colors.grey[600],
                ),
                HeaderItem(
                  images: 'images/godeals.png',
                  title: 'Arisan',
                  textcolor: Colors.grey[600],
                ),
                HeaderItem(
                  images: 'images/gopulsa.png',
                  title: 'Pulsa',
                  textcolor: Colors.grey[600],
                ),
                HeaderItem(
                  images: 'images/more-1.png',
                  title: 'Lainnya',
                  textcolor: Colors.grey[600],
                )
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 190,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          'http://fikyaisha.com/wp-content/uploads/2018/11/motif-batik-parang-curigo.jpg'),
                      fit: BoxFit.cover),
                  color: Colors.blue[800],
                  borderRadius: BorderRadius.circular(10)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Text('Batik Asli Raja Polah'),
            ),
            Text(
                'Ipsum exercitation aliqua id in irure dolor anim sint. Nostrud voluptate dolor qui nisi do. Pariatur tempor nisi sit minim ex mollit. Aliquip esse ex consectetur ea nulla eu occaecat ut exercitation reprehenderit consectetur quis deserunt commodo. Est commodo nostrud exercitation culpa do laboris ipsum sint culpa. Ut quis ea et fugiat mollit ullamco. Duis id consequat officia eu amet excepteur enim culpa excepteur ex eu.'),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                Spacer(),
                FlatButton(
                  color: Colors.green,
                  child: Text(
                    'Selengkapnya',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class HeaderItem extends StatelessWidget {
  const HeaderItem({Key key, this.images, this.title, this.textcolor})
      : super(key: key);

  final String images;
  final String title;
  final Color textcolor;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Image.asset(
          images,
          scale: 3,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: TextStyle(color: textcolor ?? Colors.white),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';

class Inboxs extends StatefulWidget {
  @override
  _InboxsState createState() => _InboxsState();
}

class _InboxsState extends State<Inboxs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 25,
          child: Image.asset('images/sukapura.png'),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
            color: Colors.black45,
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.message),
            color: Colors.black45,
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: false,
      ),
      body: Center(
        child: Text('Inbox'),
      ),
    );
  }
}
